# Job Interview challenge

This repo contains solutions for Job interview exercises.
The solution uses:
  * a Digital ocean droplet provisioned with [ansible playbook](https://gitlab.com/saico1/ansible-saico-cloud).
  * Duckdns free dns records
  * serverspec for testing
  * Docker and docker compose for build and deploy
  * [Github repo source code](https://github.com/fravega/pyecho/)

## Wokflow

Every time a new tag is created a pipeline is lauched. This pipeline will:
  * Provision runer with ssh allocated in repo variables
  * Download latest changes on github repo
  * Build and Upload image to gitlab registry
  * Deploy image on server
  * Perform tests

## Testing

Every time a new image is deployed the following test are performed:

* Check 80 and 443 ports available
* Chack latest version of image is deployed on server
* Check if url responds with 200 
