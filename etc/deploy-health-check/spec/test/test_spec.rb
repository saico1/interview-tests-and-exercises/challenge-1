require 'spec_helper'

registry = ENV['CI_REGISTRY_IMAGE']
tag = ENV['CI_COMMIT_TAG']
server = ENV['SERVER_NAME']
image = ENV['IMAGE']

describe service('sshd') do
  it { should be_enabled }
  it { should be_running }
end

describe port(443) do
  it { should be_listening }
end

describe port(80) do
  it { should be_listening }
end

describe docker_container("#{registry}/#{image}:#{tag}") do
  it { should exist }
end

describe command("curl -I https://#{server}") do
  its(:stdout) { should match /200/ }
end

